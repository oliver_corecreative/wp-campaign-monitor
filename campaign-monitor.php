<?php
/*
Plugin Name: CC Campaign Monitor
Version: 1
Plugin URI: http://corecreative.co.uk/
Author: Oliver Sadler
Author URI: http://oliversadler.co.uk/
Description: API Wrapper for Campaign Monitor subscriptions
*/

require 'inc/campaign-monitor.class.php';
require 'inc/campaign-monitor-settings.php';
require 'inc/admin/page-content.php';


CC_Campaign_Monitor::get_instance(array(
    'api_key' => get_option('cc_cm_api_key'),
    'default_list' => get_option('cc_cm_default_list')
));

add_action('admin_menu', 'cc_campaign_monitor_admin_menu');

function cc_campaign_monitor_admin_menu() {
    add_menu_page('Email Subscribers', 'Email Subscribers', 'manage_options', 'campaign-monitor/inc/admin/page.php', 'cc_campaign_monitor_admin_page', 'dashicons-email-alt', 6);
}

add_action('wp_ajax_sign-up-form', 'cc_campaign_monitor_ajax_action');
add_action('wp_ajax_nopriv_sign-up-form', 'cc_campaign_monitor_ajax_action');

add_action('wp_ajax_cm_remove_subscriber', 'cc_campaign_monitor_remove');
add_action('wp_ajax_nopriv_cm_remove_subscriber', 'cc_campaign_monitor_remove');

add_action('wp_ajax_cc-download-csv', 'cc_download_csv');
function cc_campaign_monitor_ajax_action()
{

    $validation = array(
        'email' => 'required|email'
    );


    $return = $errors = array();

    foreach ($_POST as $key => $value) {

        $value = trim($value);

        if (isset($validation[$key])) { //then do validation

            foreach (explode('|', $validation[$key]) as $rule) {
                if (!isset($errors[$key])) {
                    switch ($rule) {

                        case('required'):

                            if (empty($value))
                                $errors[$key] = '<strong>{{field}}</strong> is required.';

                            break;

                        case('email'):

                            if (!filter_var($value, FILTER_VALIDATE_EMAIL))
                                $errors[$key] = '<strong>{{field}}</strong> is not a valid email address.';

                            break;


                    }
                }
            }

        }

    }

    if (!empty($errors)) {
        echo json_encode(array(
            'status' => 'validation',
            'data' => $errors
        ));
        exit;
    }

    $result = CC_Campaign_Monitor::get_instance()->subscribe(array(
        'EmailAddress' => $_POST['email']
    ));

    if ($result === true) {
        echo json_encode(array(
            'status' => 'success',
            'data' => $result
        ));

        if(get_option('cc_cm_send_notification') == 1) {
            wp_mail(get_option('admin_email'), 'New Email Subscriber', 'New email subscriber from website: ' . $_POST['email']);
        }

    } else {
        echo json_encode(array(
            'status' => 'error',
            'message' => $result
        ));
    }
    exit;

}

function cc_campaign_monitor_remove() {

    $result = CC_Campaign_Monitor::get_instance()->unsubscribe($_REQUEST['email']);
    header("Location: " . $_SERVER['HTTP_REFERER']);
    exit;
}