<?php
/**
 * Created by PhpStorm.
 * User: valleyview
 * Date: 09/06/2014
 * Time: 11:28
 */


add_action( 'admin_init', function() {

    add_option( 'cc_cm_api_key', '');
    register_setting( 'default', 'cc_cm_api_key' );

    add_option( 'cc_cm_default_list', '');
    register_setting( 'default', 'cc_cm_default_list' );

    add_option( 'cc_cm_send_notification', '0');
    register_setting( 'default', 'cc_cm_send_notification' );

});

add_action('admin_menu', function() {
    add_options_page('Campaign Monitor Settings', 'Campaign Monitor', 'manage_options', 'campaign-monitor', function() {

        include 'campaign-monitor-settings-page.php';

    });
});