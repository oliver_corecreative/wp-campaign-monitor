<?php
/**
 * Created by Oliver Sadler - 07773691456.
 * User: iMac9
 * Date: 12/08/2014
 * Time: 08:55
 */

require_once 'api/csrest_subscribers.php';
require_once 'api/csrest_lists.php';

class CC_Campaign_Monitor {

    private $options;

    public function __construct( $params ) {

        $this->options = $params;

    }

    public static function template() {

	    ob_start();
	    require plugin_dir_path( __FILE__ ) . 'template.php';
	    return ob_get_clean();


    }

    public static function get_instance($params = array()) {
        static $singleton_instance = null;
        if($singleton_instance === null) {
            $singleton_instance = new CC_Campaign_Monitor($params);
        }

        return($singleton_instance);
    }


    public function subscribe($data, $list_id = false) {

        if(!$list_id)
            $list_id = $this->options['default_list'];

        $wrap = new CS_REST_Subscribers($list_id, array('api_key' => $this->options['api_key']));

        $data['Resubscribe'] = true;
        $result = $wrap->add($data);

        if($result->was_successful()) {
            return true;
        } else {
            return 'Error ' . $result->http_status_code . ': ' . $result->response->Message;
        }

    }


    public function unsubscribe($email_address, $list_id = false) {

        if(!$list_id)
            $list_id = $this->options['default_list'];

        $wrap = new CS_REST_Subscribers($list_id, array('api_key' => $this->options['api_key']));

        $result = $wrap->unsubscribe($email_address);

        if($result->was_successful()) {
            return true;
        } else {
            return 'Error ' . $result->http_status_code . ': ' . $result->response->Message;
        }

    }


    public function get_subscribers($list_id = false) {

        if(!$list_id)
            $list_id = $this->options['default_list'];

        $wrap = new CS_REST_Lists($list_id, array('api_key' => $this->options['api_key']));

        $result = $wrap->get_active_subscribers('', NULL, NULL, 'DATE', 'desc');

        if($result->was_successful()) {
            return $result->response->Results;
        } else {
            return 'Error ' . $result->http_status_code . ': ' . $result->response->Message;
        }

    }


    public function get_list($list_id = false) {

        if(!$list_id)
            $list_id = $this->options['default_list'];

        $wrap = new CS_REST_Lists($list_id, array('api_key' => $this->options['api_key']));

        $result = $wrap->get();

        if($result->was_successful()) {
            return $result->response;
        } else {
            return 'Error ' . $result->http_status_code . ': ' . $result->response->Message;
        }

    }

}