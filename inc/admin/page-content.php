<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 25/08/15
 * Time: 12:24
 */
function cc_campaign_monitor_admin_page() {

    // get list

    $data = CC_Campaign_Monitor::get_instance()->get_subscribers();
    $list = CC_Campaign_Monitor::get_instance()->get_list();

    ?>
    <div class="wrap">
        <h1>Email Subscribers</h1>

        <p><?= count($data); ?> subscribers in "<?= $list->Title; ?>"</p>

        <div class="tablenav top">
            <a href="<?= admin_url('admin-ajax.php'); ?>?action=cc-download-csv" class="button">Export Spreadsheet</a>
        </div>

        <p>&nbsp;</p>

        <table class="wp-list-table widefat fixed striped posts">
            <thead>
            <tr>
                <th scope="col">Email Address</th>
                <th scope="col">Date Subscribed</th>
                <th scope="col">Status</th>
                <th scope="col">Reads Email With</th>
                <th scope="col">Unsubscribe</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach($data as $subscriber): ?>
            <tr>
                <th><a href="mailto:<?= $subscriber->EmailAddress; ?>"><?= $subscriber->EmailAddress; ?></a></th>
                <td><?= $subscriber->Date; ?></td>
                <td><?= $subscriber->State; ?></td>
                <td><?= $subscriber->ReadsEmailWith; ?></td>
                <td><a href="<?= admin_url("admin-ajax.php"); ?>?action=cm_remove_subscriber&email=<?= $subscriber->EmailAddress; ?>">Remove</a></td>
            </tr>
            <?php endforeach; ?>

            </tbody>
            <tfoot>
            <tr>
                <th scope="col">Email Address</th>
                <th scope="col">Date Subscribed</th>
                <th scope="col">Status</th>
                <th scope="col">Reads Email With</th>
                <th scope="col">Unsubscribe</th>
            </tr>
            </tfoot>
        </table>

    </div>
    <?php
}
function cc_download_csv() {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="website-subscribers.csv";');

    $data = CC_Campaign_Monitor::get_instance()->get_subscribers();
    $delimiter = ',';

    // open the "output" stream
    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    $f = fopen('php://output', 'w');

    fputcsv($f, array(
        'Email Address',
        'Name',
        'Date Subscribed',
        'Status',
        'Reads Email With'
    ), $delimiter);

    foreach ($data as $line) {
        unset($line->CustomFields);
        fputcsv($f, (array)$line, $delimiter);
    }

    exit;
}