<form role="form" method="get" class="subscribe search-form form-inline" action="<?= admin_url( 'admin-ajax.php' ); ?>" data-campaign-monitor="true">
	<div class="message"></div>

	<label class="sr-only"><?php _e( 'Email Address:', 'sage' ); ?></label>

	<div class="input-group">
		<input type="email" class="form-control" name="email" placeholder="Enter your email address..." required />
	    <span class="input-group-btn">
	        <button type="submit" class="btn btn-primary">
		        Join
	        </button>
			<input type="hidden" name="action" value="sign-up-form" />
			<i class="spinner">
				<img src="<?= get_template_directory_uri() . '/dist/images/spinner.gif'; ?>" width="16" height="16" />
			</i>
	    </span>
	</div>
</form>

<?php
add_action('wp_footer', function() {
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {

			var $form = $("form[data-campaign-monitor]");
			$form.find('.spinner').hide();
			$form.submit(function( e ) {

				e.preventDefault();

				$form = $(this);
				var $wrapper = $form;

				$form.find('button[type=submit]').hide();
				$form.find('.spinner').show();

				$.post($form.attr("action"), $form.serialize(), function(data) {

					$form.find('.spinner').hide();
					$form.find('button[type=submit]').show();

					if(data.status === "validation") {

						var _html = "<p>Please check that all fields are entered correctly.</p><ul>";

						$.each(data.data, function(key, message) {
							message = message.replace('{{field}}', ucfirst(key.replace(new RegExp("_", "g"), ' ')) );
							_html = _html + '<li>'+message+'</li>';
						});
						_html = _html + '</ul>';


						$wrapper.removeClass('validation-success');
						$wrapper.removeClass('validation-error');
						$wrapper.addClass('validation-warning');

						$wrapper.addClass('show-message');

						$wrapper.find('.message').addClass('alert');

						$wrapper.find('.message').removeClass('alert-success');
						$wrapper.find('.message').removeClass('alert-danger');
						$wrapper.find('.message').addClass('alert-warning');

						$wrapper.find('.message').html(_html);


					} else if (data.status === "success") {

						$wrapper.addClass('validation-success');
						$wrapper.removeClass('validation-error');
						$wrapper.removeClass('validation-warning');

						$wrapper.addClass('show-message');

						$wrapper.find('.message').addClass('alert');

						$wrapper.find('.message').addClass('alert-success');
						$wrapper.find('.message').removeClass('alert-danger');
						$wrapper.find('.message').removeClass('alert-warning');

						$wrapper.find('.message').html('<p>Thank you for signing up</p>');

						setTimeout(function(){
							//window.location = "/thank-you/";

							$wrapper.find('.message').removeClass('alert-success');
							$wrapper.find('.message').removeClass('alert-danger');
							$wrapper.find('.message').removeClass('alert-warning');
							$wrapper.find('.message').removeClass('alert');

							$wrapper.find('.message').html('');

						}, 2500); // go to the success/thank you page

					} else {

						$wrapper.addClass('validation-error').addClass('show-message').find('.message').addClass('alert').addClass('alert-danger');

						$wrapper.removeClass('validation-success');
						$wrapper.addClass('validation-error');
						$wrapper.removeClass('validation-warning');

						$wrapper.addClass('show-message');

						$wrapper.find('.message').addClass('alert');

						$wrapper.find('.message').removeClass('alert-success');
						$wrapper.find('.message').addClass('alert-danger');
						$wrapper.find('.message').removeClass('alert-warning');

						console.log(data.message);
						$wrapper.find('.message').html('<p>An error occurred, please try again later</p>');

					}

				}, 'json');


			});

		});
	</script>
	<?php
}, 100);
?>