<div class="wrap">
<h2>Campaign Monitor Settings</h2>

<form method="post" action="options.php">
<?php settings_fields( 'default' ); ?>

<table class="form-table">
<tbody>
<tr>
    <th scope="row"><label for="cc_cm_api_key">API Key </label></th>
    <td><input name="cc_cm_api_key" type="text" id="cc_cm_api_key" value="<?= get_option('cc_cm_api_key'); ?>" class="regular-text code"></td>
</tr>
<tr>
    <th scope="row"><label for="cc_cm_default_list">Default List ID </label></th>
    <td><input name="cc_cm_default_list" type="text" id="cc_cm_default_list" value="<?= get_option('cc_cm_default_list'); ?>" class="regular-text code"></td>
</tr>
<tr>
    <th scope="row"><label for="cc_cm_send_notification">Send email notification? </label></th>
    <td><input type="checkbox" name="cc_cm_send_notification" value="1" <?php checked( get_option('cc_cm_send_notification'), 1 ); ?> /></td>
</tr>
</tbody>
</table>


    <?php submit_button(); ?>

</div>